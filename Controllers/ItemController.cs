﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SEO_System.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SEO_System.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    public class ItemController : Controller
    {
        private Item itemObj = new Item();

        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        [Route("~/Item/TestContract")]
        public IActionResult TestContract()
        {
            return View("test_contract");
        }

        [HttpPost]
        [Route("~/Item/List")]
        public IActionResult List()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            string search = Request.Form["search[value]"].FirstOrDefault();
            var data = itemObj.List(int.Parse(length), int.Parse(start), search);
            int recordsTotal = itemObj.GetAllItem().Count;
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        [HttpGet]
        [Route("~/Item/ListOld")]
        public ActionResult ListOld()
        {
            List<Item> data = new Item().GetAllItem();
            return Json(new { data });
        }

        [HttpDelete]
        [Route("~/Item/Delete/{ItemId}")]
        public JsonResult Delete(int ItemId)
        {
            return Json(new { result = itemObj.Delete(ItemId) });
        }

        [HttpPost]
        [Route("~/Item/Update/")]
        public JsonResult Update(Item Item)
        {
            return Json(new { result = itemObj.UpdateOrInsert(Item) });
        }

        [HttpGet]
        [Route("~/Item/Get")]
        public ActionResult List(int ItemId, string ItemName, string ItemDesc,int gameid)
        {
            List<Item> data = itemObj.Get(ItemId, ItemName, ItemDesc,gameid);
            return Json(new { data });
        }

        [Route("~/Item/upload_file")]
        [HttpPost]
        public async Task<IActionResult> upload_file(IFormFile file)
        {
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            if (file == null || file.Length == 0)
                return Content("file not selected");
            string sFileWillSave = file.FileName;
            string fileDirect = @"/home/amnhacsaigon/dotnet/hag/wwwroot/data-file/item_images/";
            if (isDev)
            {
                fileDirect = @"C:\data\";
            }
            if (!Directory.Exists(fileDirect))
            {
                Directory.CreateDirectory(fileDirect);
            }
            var path = Path.Combine(Directory.GetCurrentDirectory(), fileDirect, sFileWillSave);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            return Json(new { file = "https://amostars.io/data-file/item_images/" + sFileWillSave });
        }
    }
}