﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SEO_System.Models;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    public class TransactionController : Controller
    {
        private Transaction TransactionObj = new Transaction();

        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        [Route("~/Transaction/List")]
        public IActionResult List()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            string search = Request.Form["search[value]"].FirstOrDefault();
            var data = TransactionObj.List(int.Parse(length), int.Parse(start), search);
            int recordsTotal = TransactionObj.GetAllTransaction().Count;
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        [HttpGet]
        [Route("~/Transaction/ListOld")]
        public ActionResult ListOld()
        {
            List<Transaction> data = new Transaction().GetAllTransaction();
            return Json(new { data });
        }

        [HttpDelete]
        [Route("~/Transaction/Delete/{tranId}")]
        public JsonResult Delete(int tranId)
        {
            return Json(new { result = TransactionObj.Delete(tranId) });
        }

        [HttpPost]
        [Route("~/Transaction/Update/")]
        public JsonResult Update(Transaction Transaction)
        {
            return Json(new { result = TransactionObj.UpdateOrInsert(Transaction)});
        }

        [HttpGet]
        [Route("~/Transaction/Get")]
        public ActionResult List(int tranId, string blockHash, string blockNumber)
        {
            List<Transaction> data = TransactionObj.Get(tranId, blockHash, blockNumber);
            return Json(new { data });
        }
    }
}