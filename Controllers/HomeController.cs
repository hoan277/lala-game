﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SEO_System.Models;

namespace SEO_System.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            if (string.IsNullOrEmpty(HttpContext.Session.GetString("userName")))
            {
                return RedirectToAction("Login", "User");
            }
            return View();
        }

        [HttpGet]
        [Route("~/Home/CountAll")]
        public JsonResult CountAll()
        {
            var dict = new Home().CountAll();
            return Json(new { data = dict });
        }
    }
}