﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SEO_System.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    public class ScoreController : Controller
    {
        private Score scoreObj = new Score();

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("~/Score/List")]
        public IActionResult List()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            string search = Request.Form["search[value]"].FirstOrDefault();
            var data = scoreObj.List(int.Parse(length), int.Parse(start), search);
            int recordsTotal = scoreObj.GetAllScore().Count;
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        [HttpGet]
        [Route("~/Score/ListOld")]
        public ActionResult ListOld()
        {
            List<Score> data = new Score().GetAllScore();
            return Json(new { data });
        }

        [HttpDelete]
        [Route("~/Score/Delete/{scoreId}")]
        public JsonResult Delete(int scoreId)
        {
            return Json(new { result = scoreObj.Delete(scoreId) });
        }

        [HttpPost]
        [Route("~/Score/Update/")]
        public JsonResult Update(Score score)
        {
            return Json(new { result = scoreObj.UpdateOrInsert(score) });
        }

        [HttpGet]
        [Route("~/Score/Get")]
        public ActionResult List(int scoreId, string gameSlug, int gameLevel, int gameScore, DateTime scoreCreated, DateTime scoreUpdated)
        {
            List<Score> data = scoreObj.Get(scoreId, gameSlug, gameLevel, gameScore, scoreCreated, scoreUpdated);
            return Json(new { data });
        }
        [HttpPost]
        [Route("~/Score/UpdateScore")]
        public JsonResult UpdateScore(Score score)
        {
            return Json(new { result = scoreObj.UpdateOrInsert(score) });
        }
    }
}