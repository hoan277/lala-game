﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SEO_System.Models;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    public class CategoryController : Controller
    {
        private Category cateObj = new Category();

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("~/Category/List")]
        public IActionResult List()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            string search = Request.Form["search[value]"].FirstOrDefault();
            var data = cateObj.List(int.Parse(length), int.Parse(start), search);
            int recordsTotal = cateObj.GetAllCategory().Count;
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        [HttpGet]
        [Route("~/Category/ListOld")]
        public ActionResult ListOld()
        {
            List<Category> data = new Category().GetAllCategory();
            return Json(new { data });
        }

        [HttpDelete]
        [Route("~/Category/Delete/{cateId}")]
        public JsonResult Delete(int cateId)
        {
            return Json(new { result = cateObj.Delete(cateId) });
        }

        [HttpPost]
        [Route("~/Category/Update/")]
        public JsonResult Update(Category cate)
        {
            return Json(new { result = cateObj.UpdateOrInsert(cate) });
        }
    }
}