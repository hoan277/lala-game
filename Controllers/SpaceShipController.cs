﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SEO_System.Models;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    public class SpaceShipController : Controller
    {
        private SpaceShip ssObj = new SpaceShip();

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("~/SpaceShip/List")]
        public IActionResult List()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            string search = Request.Form["search[value]"].FirstOrDefault();
            var data = ssObj.List(int.Parse(length), int.Parse(start), search);
            int recordsTotal = ssObj.GetAllSpaceShip().Count;
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        [HttpGet]
        [Route("~/SpaceShip/ListOld")]
        public ActionResult ListOld()
        {
            List<SpaceShip> data = new SpaceShip().GetAllSpaceShip();
            return Json(new { data });
        }

        [HttpDelete]
        [Route("~/SpaceShip/Delete/{ssId}")]
        public JsonResult Delete(int ssId)
        {
            return Json(new { result = ssObj.Delete(ssId) });
        }

        [HttpPost]
        [Route("~/SpaceShip/Update/")]
        public JsonResult Update(SpaceShip ss)
        {
            return Json(new { result = ssObj.UpdateOrInsert(ss) });
        } 
        [HttpGet]
        [Route("~/SpaceShip/Get")]
        public JsonResult Get(int ssId, string ssName)
        {
            return Json(new { result = ssObj.Get(ssId, ssName) });
        }
    }
}