﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SEO_System.Models;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    public class GameController : Controller
    {
        private Game gameObj = new Game();

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("~/Game/List")]
        public IActionResult List()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            string search = Request.Form["search[value]"].FirstOrDefault();
            var data = gameObj.List(int.Parse(length), int.Parse(start), search);
            int recordsTotal = gameObj.GetAllGame().Count;
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        [HttpGet]
        [Route("~/Game/ListOld")]
        public ActionResult ListOld()
        {
            List<Game> data = new Game().GetAllGame();
            return Json(new { data });
        }

        [HttpDelete]
        [Route("~/Game/Delete/{gameId}")]
        public JsonResult Delete(int gameId)
        {
            return Json(new { result = gameObj.Delete(gameId) });
        }

        [HttpPost]
        [Route("~/Game/Update/")]
        public JsonResult Update(Game game)
        {
            return Json(new { result = gameObj.UpdateOrInsert(game) });
        }

        [HttpGet]
        [Route("~/Game/Get")]
        public ActionResult List(int gameId, string gameName, string gameDesc, string gameSlug, string gameStatus, int cateId)
        {
            List<Game> data = gameObj.Get(gameId, gameName, gameDesc, gameSlug, gameStatus, cateId);
            return Json(new { data });
        }
    }
}