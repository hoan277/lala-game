﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SEO_System.Models;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    public class RoadMapController : Controller
    {
        private RoadMap rmObj = new RoadMap();

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("~/RoadMap/List")]
        public IActionResult List()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            string search = Request.Form["search[value]"].FirstOrDefault();
            var data = rmObj.List(int.Parse(length), int.Parse(start), search);
            int recordsTotal = rmObj.GetAllRoadMap().Count;
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        [HttpGet]
        [Route("~/RoadMap/ListOld")]
        public ActionResult ListOld()
        {
            List<RoadMap> data = new RoadMap().GetAllRoadMap();
            return Json(new { data });
        }

        [HttpDelete]
        [Route("~/RoadMap/Delete/{rmId}")]
        public JsonResult Delete(int rmId)
        {
            return Json(new { result = rmObj.Delete(rmId) });
        }

        [HttpPost]
        [Route("~/RoadMap/Update/")]
        public JsonResult Update(RoadMap rm)
        {
            return Json(new { result = rmObj.UpdateOrInsert(rm) });
        }
        [HttpGet]
        [Route("~/RoadMap/Get")]
        public JsonResult Get(int rmId, string rmName)
        {
            return Json(new { result = rmObj.Get(rmId, rmName) });
        }
    }
}