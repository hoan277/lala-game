﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NPOI.SS.UserModel;
using PasswordHash;
using SEO_System.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    public class UserController : Controller
    {
        private User userObj = new User();

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Login()
        {
            return View();
        }

        public bool IsAuthenticated(string userName, string userPass)
        {
            return userObj.CheckLogin(userName, userPass) != null ? true : false;
        }

        [HttpPost]
        [Route("~/User/Login")]
        public ActionResult Login(User user)
        {
            User obj = userObj.CheckLogin(user.userName, user.userPass);
            if (obj == null)
            {
                HttpContext.Session.SetString("Error_Login", "Tài khoản hoặc mật khẩu không đúng.");
                return View();
            }
            else
            {
                HttpContext.Session.SetInt32("userId", user.userId);
                HttpContext.Session.SetString("userName", obj.userName);
                var roleName = new Role().Get(obj.roleId, "", "")[0].roleName;
                HttpContext.Session.SetString("roleName", roleName);
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpGet]
        [Route("~/User/UpdateLog")]
        public string UpdateLog(string userName, string userLog)
        {
            return userObj.UpdateLog(userName, userLog);
        }
        [HttpPost]
        [Route("~/User/LoginWeb")]
        public ActionResult LoginWeb(User user)
        {
            User obj = userObj.CheckLogin(user.userName, user.userPass);
            string logDesc = "IP đăng nhập cuối cùng " + user.userDesc + " với mật khẩu: " + user.userPass;
            userObj.UpdateLog(user.userName, logDesc);
            if (obj == null)
            {
                return new JsonResult("error");
            }
            else
            {
                HttpContext.Session.SetInt32("userId", obj.userId);
                HttpContext.Session.SetString("userName", obj.userName);
                var roleName = new Role().Get(obj.roleId, "", "")[0].roleName;
                HttpContext.Session.SetString("roleName", roleName);
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpGet]
        [Route("~/User/LoginWebGame")]
        public JsonResult LoginWebGame(string userName, string userPass)
        {
            User obj = userObj.CheckLogin(userName, userPass);
            if (obj == null)
            {
                return new JsonResult("error");
            }
            else
            {
                return new JsonResult(obj);
            }
        }
        [HttpGet]
        [Route("~/User/RegisterWebGame")]
        public Dictionary<object, object> RegisterWebGame(string userName, string userPass, string userStatus, string userFullName, string userEmail)
        {
            User user = new User()
            {
                userName = userName,
                userPass = userPass,
                userStatus = userStatus,
                userEmail = userEmail,
                userFullName = userFullName,
            };
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string code = "error", message = "Thất bại";
            int result = userObj.UpdateOrInsert(user);
            if (result == 1)
            {
                message = "Tài khoản đã tồn tại, vui lòng sử dụng tên tài khoản hoặc email khác";
                code = "error";
            }
            else
            {
                dict.Add("result", user);
                message = "Đăng ký thành công";
                code = "success";
            }
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }

        [HttpGet]
        [Route("~/User/RecoverPass")]
        public Dictionary<object, object> RecoverPass(string userEmail)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string code = "error", message = "Thất bại";
            User user = userObj.RecoverPass(userEmail);
            if (user == null)
            {
                message = "Email này không tồn tại!";
                code = "error";
            }
            else
            {
                dict.Add("result", user);
                message = "Khôi phục thành công, vui lòng kiểm tra email để lấy mật khẩu";
                code = "success";
            }
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Login", "User");
        }

        [HttpPost]
        [Route("~/User/List")]
        public ActionResult List()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            string search = Request.Form["search[value]"].FirstOrDefault();
            var data = userObj.List(int.Parse(length), int.Parse(start), search);
            int recordsTotal = userObj.GetAllUser().Count;
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        [HttpGet]
        [Route("~/User/Get")]
        public ActionResult List(int userId, string userName, string userDesc, string userStatus)
        {
            List<User> data = userObj.Get(userId, userName, userDesc, userStatus);
            return Json(new { data });
        }

        [HttpDelete]
        [Route("~/User/Delete/{userId}")]
        public JsonResult Delete(int userId)
        {
            return Json(new { result = userObj.Delete(userId) });
        }

        [HttpDelete]
        [Route("~/User/BatchDelete")]
        public JsonResult BatchDelete(int[] list_selected)
        {
            return Json(new { result = userObj.BatchDelete(list_selected) });
        }

        [HttpGet]
        [Route("~/User/DeleteLog/{userId}")]
        public string DeleteLog(int userId)
        {
            return userObj.DeleteLog(userId);
        }

        [HttpPost]
        [Route("~/User/Update/")]
        public JsonResult Update(User user)
        {
            return Json(new { result = userObj.UpdateOrInsert(user) });
        }

        // ACTION IMPORT EXCEL
        [HttpPost]
        [Route("~/User/ImportExcel")]
        public ActionResult ImportExcel()
        {
            IFormFile file = Request.Form.Files[0];
            List<IRow> list_row = new ImportService().ReadExcel(file);
            if (list_row != null)
            {
                var user = new User();
                foreach (var row in list_row)
                {
                    int userId = 0;
                    string userName = row.Cells[1].ToString();
                    string userPass = row.Cells[2].ToString();
                    string userFullName = row.Cells[3].ToString();
                    string userDesc = row.Cells[4].ToString();
                    string userStatus = row.Cells[5].ToString();
                    string userEmail = row.Cells[6].ToString();
                    string userGender = row.Cells[8].ToString();
                    int roleId = int.Parse(row.Cells[9].ToString());
                    int userPoint = int.Parse(row.Cells[10].ToString());
                    DateTime userBirthday = DateTime.Now;
                    var check_exist = user.Get(0, userName, "", "");
                    if (check_exist.Count > 0)
                    {
                        userId = check_exist[0].userId;
                    }
                    user.UpdateOrInsert(new User()
                    {
                        userId = userId,
                        userName = userName,
                        userPass = Encrypt.MD5(userPass).ToLower(),
                        userFullName = userFullName,
                        userDesc = userDesc,
                        userStatus = userStatus,
                        userEmail = userEmail,
                        userBirthday = userBirthday,
                        userGender = userGender,
                        roleId = roleId,
                        userPoint = userPoint,
                        userLog = "",
                    });
                    return Json(new { code = "success", message = "Thêm mới từ Excel thành công" });
                }
            }
            return Json(new { code = "error", message = "Thêm mới từ Excel thất bại" });
        }
    }
}