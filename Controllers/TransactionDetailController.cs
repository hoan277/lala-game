﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SEO_System.Models;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    public class TransactionDetailController : Controller
    {
        private TransactionDetail TransactionDetailObj = new TransactionDetail();

        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        [Route("~/TransactionDetail/List")]
        public IActionResult List()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            string search = Request.Form["search[value]"].FirstOrDefault();
            var data = TransactionDetailObj.List(int.Parse(length), int.Parse(start), search);
            int recordsTotal = TransactionDetailObj.GetAllTransactionDetail().Count;
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        [HttpGet]
        [Route("~/TransactionDetail/ListOld")]
        public ActionResult ListOld()
        {
            List<TransactionDetail> data = new TransactionDetail().GetAllTransactionDetail();
            return Json(new { data });
        }

        [HttpDelete]
        [Route("~/TransactionDetail/Delete/{tranId}")]
        public JsonResult Delete(int tranId)
        {
            return Json(new { result = TransactionDetailObj.Delete(tranId) });
        }

        [HttpPost]
        [Route("~/TransactionDetail/Update/")]
        public JsonResult Update(TransactionDetail TransactionDetail)
        {
            return Json(new { result = TransactionDetailObj.UpdateOrInsert(TransactionDetail) });
        }

        [HttpGet]
        [Route("~/TransactionDetail/Get")]
        public ActionResult List(int tranId, string blockHash, string blockNumber)
        {
            List<TransactionDetail> data = TransactionDetailObj.Get(tranId, blockHash, blockNumber);
            return Json(new { data });
        }
    }
}