﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SEO_System.Models;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    public class ConfigGameController : Controller
    {
        private ConfigGame cateObj = new ConfigGame();

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("~/ConfigGame/List")]
        public IActionResult List()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            string search = Request.Form["search[value]"].FirstOrDefault();
            var data = cateObj.List(int.Parse(length), int.Parse(start), search);
            int recordsTotal = cateObj.GetAllConfigGame().Count;
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        [HttpGet]
        [Route("~/ConfigGame/ListOld")]
        public ActionResult ListOld()
        {
            List<ConfigGame> data = new ConfigGame().GetAllConfigGame();
            return Json(new { data });
        }

        [HttpDelete]
        [Route("~/ConfigGame/Delete/{cgKey}")]
        public JsonResult Delete(string cgKey)
        {
            return Json(new { result = cateObj.Delete(cgKey) });
        }

        [HttpPost]
        [Route("~/ConfigGame/Update/")]
        public JsonResult Update(ConfigGame cate)
        {
            return Json(new { result = cateObj.UpdateOrInsert(cate) });
        } 
        [HttpPost]
        [Route("~/ConfigGame/upload_img/{data_model}")]
        public JsonResult upload_img(string data_model)
        {
            IFormFile file = Request.Form.Files[0];
            return Json(new { result = cateObj.upload_img(file, data_model) });
        }
    }
}