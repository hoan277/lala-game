﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SEO_System.Models;
using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Controllers
{
    public class LandingPageController : Controller
    {
        [HttpGet]
        [Route("~/home")]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        [Route("~/home1")]
        public IActionResult Index1()
        {
            return View("Index1");
        }
        [HttpGet]
        [Route("~/home/detail/{itemId}")]
        public IActionResult Detail(int itemId)
        {
            if (itemId > 0)
            {
                using (var db = DatabaseUtils.OpenConnection())
                {
                    var query = db.Select(db.From<Item>().Where(e => e.itemId == itemId)).FirstOrDefault();
                    return View("detailItem", query);
                }
            }
            return null;
        }
    }
}