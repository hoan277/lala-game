﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SEO_System.Models;
using System.Collections.Generic;

namespace SEO_System.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    public class RoleController : Controller
    {
        private Role roleObj = new Role();

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route("~/Role/List")]
        public ActionResult List(int length, int start, string search)
        {
            List<Role> data = roleObj.GetAllRole();
            return Json(new { data });
        }

        [HttpGet]
        [Route("~/Role/Get")]
        public ActionResult List(int roleId, string roleName, string roleDesc)
        {
            List<Role> data = roleObj.Get(roleId, roleName, roleDesc);
            return Json(new { data });
        }

        [HttpDelete]
        [Route("~/Role/Delete/{roleId}")]
        public JsonResult Delete(int roleId)
        {
            return Json(new { result = roleObj.Delete(roleId) });
        }

        [HttpPost]
        [Route("~/Role/Update/")]
        public JsonResult Update(Role role)
        {
            return Json(new { result = roleObj.UpdateOrInsert(role) });
        }
    }
}