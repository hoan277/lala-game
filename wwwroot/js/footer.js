﻿$(document).ready(function () {
    //#region  ========================= Lấy IP của người dùng đăng nhập =========================
    if ($("#userDesc")) {
        $.ajax({
            url: "https://api.ipify.org/?format=json",
            method: "GET",
            success: function (data) {
                $("#userDesc").html(data.ip);
            },
            error: function () {
                $("#userDesc").html("127.0.0.1");
            },
        });
    }
    //#endregion
    //#region  ========================= Active menu =========================
    var curent_url = window.location.pathname;
    if (!curent_url.includes("Edit")) {
        var urlRegExp = new RegExp(curent_url.replace(/\/$/, '') + "$");
        $('ul#main-menu li a').each(function () {
            if (urlRegExp.test(this.href.replace(/\/$/, ''))) {
                $(this).parent().addClass('active');
            }
        });
    } else {
        // Set active menu
        if (curent_url.includes("Timeline/Edit")) {
            $(".timeline").addClass("active");
        }
    }
    //#endregion
    //#region  ========================= Style cho datatables =========================
    setInterval(function (mutations) {
        if ($("table tbody tr").length) {
            if (!$("div.top").hasClass("row")) {
                $("div.top").addClass("row");
                $("div.bottom").addClass("row");
                $("div.dataTables_wrapper div.dataTables_filter").addClass("col-md-3");
                $("div.dataTables_wrapper div.dataTables_length").addClass("col-md-2");
                $("div.dataTables_wrapper div.dataTables_info").addClass("col-md-3");
                $("div.dataTables_wrapper div.dataTables_paginate").addClass("col-md-4");
                $(".dt-buttons").addClass("btn-group-vertical");
            }
        }
    }, 1000);
    //#endregion
    //#region  ========================= Function Login =========================
    if (localStorage.getItem("userName") != "") { $("#userName").val(localStorage.getItem("userName")); }
    if (localStorage.getItem("userPass") != "") { $("#userPass").val(localStorage.getItem("userPass")); }
    if (localStorage.getItem("cbRemember")) { $("#cbRemember").prop("checked", localStorage.getItem("cbRemember")); }
    $("#btnLogin").click(function (event) {
        let userName = $("#userName").val();
        let userPass = $("#userPass").val();
        let userDesc = $("#userDesc").html();
        let cbRemember = $("#cbRemember").prop('checked');
        if (userName.length <= 0) {
            hcShowToast("Thông báo", "Chưa nhập tài khoản", "warning", "dark", "5000");
            return;
        }
        if (userPass.length <= 0) {
            hcShowToast("Thông báo", "Chưa nhập mật khẩu", "warning", "dark", "5000");
            return;
        }
        let data_user = {
            userName: userName,
            userPass: userPass,
            userDesc: userDesc,
        }
        hcShowToast("Đang đăng nhập...", "", "warning", "dark", Math.floor(Math.random() * 2000));
        setTimeout(function () {
            $.ajax({
                url: "/User/LoginWeb",
                method: "POST",
                data: data_user
                , success: function (data) {
                    if (data != "error") {
                        localStorage.setItem("userName", userName);
                        localStorage.setItem("userPass", userPass);
                        localStorage.setItem("cbRemember", cbRemember);
                        location.href = "/Home/Index";
                    } else {
                        hcShowToast("Thông báo", "Thông tin tài khoản không chính xác", "warning", "dark", "5000");
                    }
                }, error: function (xhr) {
                    console.log(xhr.responseText);
                }
            });
        }, Math.floor(Math.random() * 500));
    });
    $("#btnLogout").click(function () {
        $.post("/User/Logout");
    });
    $("#frmLogin input").on("keyup", function (e) {
        if (e.key === 'Enter' || e.keyCode === 13) {
            $("#btnLogin").trigger('click');
        }
    });
    //#endregion
    //#region  ========================= SETUP HOTKEY =========================
    $.key('esc', function () {
        $(".full-screen").trigger("click");
    });
    $.key('shift+tab', function () {
        $(".btn-toggle-fullwidth").trigger("click");
    });
    $.key('ctrl+1', function () {
        location.href = "/Timeline/Index";
    });
    $.key('ctrl+2', function () {
        location.href = "/Link/Index";
    });
    $.key('ctrl+3', function () {
        location.href = "/Account/Index";
    });
    $.key('ctrl+4', function () {
        location.href = "/VPS/Index";
    });
    $.key('ctrl+5', function () {
        location.href = "/User/Index";
    });
    $.key('ctrl+6', function () {
        location.href = "/Role/Index";
    });
    $.key('ctrl+q', function () {
        window.location.href = "/User/Logout";
    });
    $.key('up', function () {
        $(".upArrow").trigger('click');
    });
    $.key('down', function () {
        $(".downArrow").trigger('click');
    });
    $.key('left', function () {
        $(".leftArrow").trigger('click');
    });
    $.key('right', function () {
        $(".rightArrow").trigger('click');
    });
    //#endregion
    //#region  ========================= TẢI FILE MẪU VÀ IMPORT EXCEL =========================
    $('input#file').change(function (ev) {
        $('#btnImportExcel').attr("disabled", ($('#file').val().length == 0) ? true : false);
    });
    $("#btnSampleFile").click(function () {
        window.open("/data-file/sample/" + $(this).data("file"));
    });

    $("#btnImportExcel").click(function () {
        let data_model = $(this).data("model");
        let fileExtension = ['xls', 'xlsx'];
        let filename = $('#file').val();
        if (filename.length == 0) {
            hcShowToast("Cảnh báo", "Không chọn file thì import kiểu gì.", "warning", "dark", 4000);
            return false;
        }
        else {
            var extension = filename.replace(/^.*\./, '');
            if ($.inArray(extension, fileExtension) == -1) {
                hcShowToast("Cảnh báo", "Chỉ nhận file .xls, xlsx thôi ạ, chọn file gì đấy", "warning", "dark", 4000);
                return false;
            }
        }
        let fd = new FormData();
        let fileUpload = $("#file").get(0);
        let files = fileUpload.files;
        fd.append(files[0].name, files[0]);
        $.ajax({
            url: "/" + data_model + "/ImportExcel",
            method: "POST",
            data: fd,
            processData: false,
            contentType: false,
            success: function (data) {
                if (data.code == "success") {
                    hcShowToast("Thông báo", "Thêm mới từ Excel thành công", "success", "dark", 4000);
                    $('.table').DataTable().ajax.reload();
                    $("input[type=file]").val("");
                    $('input#file').trigger("change");
                } else {
                    hcShowToast("Lỗi", data.message, "error", "dark", 10000);
                }
            },
            error: function (xhr) {
                hcShowToast("Cảnh báo", "Lỗi API ImportExcel, vui lòng xem lại Log", "warning", "dark", 4000);
                console.log(xhr.responseText);
            },
        });
    });
    //#endregion
    //#region  ========================= ĐIỀU HƯỚNG =========================
    $(document).on({
        click: function () {
            $(this).html($(this).attr("title"));
            $(".rightArrow").trigger("click");
            if ($(this).width() > 250) {
                $(this).css("width", "250px");
                return;
            }
            $(this).css("width", "100%");
        }
    }, ".viewUserAgent");

    $(document).on({
        click: function () {
            $(".table-responsive").animate({ scrollLeft: 2000 }, 500);
            $(".table").animate({ scrollLeft: 2000 }, 500);
        }
    }, ".rightArrow");
    $(document).on({
        click: function () {
            $(".table-responsive").animate({ scrollLeft: 0 }, 500);
        }
    }, ".leftArrow");
    $(document).on({
        click: function () {
            $('body,html').animate({ scrollTop: 0 }, 800);
        }
    }, ".upArrow");
    $(document).on({
        click: function () {
            $('body,html').animate({ scrollTop: 2000 }, 800);
        }
    }, ".downArrow");
    $(document).on({
        click: function () {
            $('body,html').animate({ scrollTop: $(window).height() / 2 }, 800);
        }
    }, ".centerArrow");
    $(".btnHelp").click(function () {
        let help_html = '<ul style="width:300px" id="help" class="list-groupa">'
        help_html += '<li class="list-group-item d-flex justify-content-between align-items-center">Full màn hình<span class="badge badge-primary badge-pill">ESC</span></li>';
        help_html += '<li class="list-group-item d-flex justify-content-between align-items-center">Thoát<span class="badge badge-primary badge-pill">Ctrl + Q</span></li>';
        help_html += '<li class="list-group-item d-flex justify-content-between align-items-center">Ẩn / hiện Menu<span class="badge badge-primary badge-pill">Shift + Tab</span></li>';
        help_html += '<li class="list-group-item d-flex justify-content-between align-items-center separator">Timeline</li>';
        help_html += '<li class="list-group-item d-flex justify-content-between align-items-center">Nhân bản<span class="badge badge-primary badge-pill">Alt + C</span></li>';
        help_html += '<li class="list-group-item d-flex justify-content-between align-items-center">Thêm mới<span class="badge badge-primary badge-pill">Insert</span></li>';
        help_html += '<li class="list-group-item d-flex justify-content-between align-items-center">Hủy thêm mới<span class="badge badge-primary badge-pill">Alt + Z</span></li>';
        help_html += '<li class="list-group-item d-flex justify-content-between align-items-center">Lưu<span class="badge badge-primary badge-pill">Alt + S</span></li>';
        help_html += '<li class="list-group-item d-flex justify-content-between align-items-center">Xóa<span class="badge badge-primary badge-pill">Delete</span></li>';
        help_html += '<li class="list-group-item d-flex justify-content-between align-items-center separator">Điều hướng</li>';
        help_html += '<li class="list-group-item d-flex justify-content-between align-items-center">';
        help_html += '<span class="badge badge-primary badge-pill"><i class="fa fa-arrow-up" aria-hidden="true"></i> Lên</span>';
        help_html += '<span class="badge badge-primary badge-pill"><i class="fa fa-arrow-down" aria-hidden="true"></i> Xuống</span>';
        help_html += '<span class="badge badge-primary badge-pill"><i class="fa fa-arrow-left" aria-hidden="true"></i> Trái</span>';
        help_html += '<span class="badge badge-primary badge-pill"><i class="fa fa-arrow-right" aria-hidden="true"></i> Phải</span>';
        help_html += '</li>';

        help_html += '    </ul>';
        hcShowToast("HOTKEY", help_html, "warning", "dark", 50000);
    });

    //#endregion
    // #region ===================================== Cancel Add New =====================================
    $(document).on({
        click: function () {
            $(this).parent().parent().remove();
        }
    }, ".btnCancelAddNew");
    //if ($('[title]').length > 0) {
    //    setTimeout(function () { $('[title]').tooltip({ placement: "auto" }); }, 2000);
    //}
    // #endregion ===================================== Cancel Add New =====================================

    // #region ===================================== btnUpdateStatus =====================================
    $('#btnUpdateStatus').click(function () {
        let data_Model = $(this).data('model');
        let list_selected = [];
        $("tr.selected").each(function () {
            list_selected.push($(this).attr("id"));
        });
        if (list_selected.length > 0) {
            Swal.fire({
                title: 'Bạn có chắc không?',
                html: 'Cập nhật trạng thái cho <strong>' + list_selected.length + "</strong> " + data_Model,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Cập nhật!',
                cancelButtonText: 'Hủy!'
            }).then((result) => {
                if (result.isConfirmed) {
                    let list_selected_str = list_selected.toString();
                    $.ajax({
                        url: '/' + data_Model + '/BatchUpdateStatus/',
                        type: "POST",
                        data: {
                            list_selected: list_selected,
                            status: $('#quickStatus').val()
                        }
                    }).done(function (data) {
                        hcShowToast("Thông báo", "Cập nhật trạng thái cho ID " + list_selected_str + " thành công ", "success", "dark", 4000);
                        if ($('#btnFilter').length > 0) {
                            $('.icon-refresh').trigger('click');
                        } else { 
                            $('.table').DataTable().ajax.reload();
                        }
                    }).fail(function (jqXHR) {
                        swal("Có lỗi", jqXHR.status, "error");
                    });
                }
                list_selected = [];
            });

        } else {
            hcShowToast("Cảnh báo", "Chưa chọn ID để cập nhật trạng thái", "warning", "dark", 4000);
        }
    });
    // #endregion ===================================== btnUpdateStatus =====================================
    // #region ===================================== Cancel Add New =====================================
   
    setTimeout(function () {
        if ($("#userId").length > 0) {
            if (localStorage.getItem("userId") != "") {
                $("#userId").val(localStorage.getItem("userId"));
            }
        }
        $('.dt-buttons a').removeClass('btn-primary').removeClass('btn-round');
        $('.dt-buttons a:nth-child(1)').addClass('btn-danger');
        $('.dt-buttons a:nth-child(2)').addClass('btn-secondary');
        $('.dt-buttons a:nth-child(3)').addClass('btn-dark');
        $('.dt-buttons a:nth-child(4)').addClass('btn-primary');
        $('.dt-buttons a:nth-child(5)').addClass('btn-success');
        $('.dt-buttons a:nth-child(6)').addClass('btn-warning');
        $('.dt-buttons a:nth-child(7)').addClass('btn-dark');
        $('.dt-buttons a:nth-child(8)').addClass('btn-danger');
        $('.dt-buttons *').tooltip({ placement: "left" });
    }, 5000);
    $('nav.menu').mouseover(function () {
        $('nav.menu *').tooltip({ placement: "auto" });
    });
    $.key('insert', function () {
        $("#addRow").trigger("click");
    });
    $.key('delete', function () {
        $("#btnDelete").trigger("click");
    });
    $.key('alt+s', function () {
        $('tr.adding button#insert').trigger("click");
    });
    $.key('alt+z', function () {
        $("tr.adding").remove();
    });
    $.key('alt+c', function () {
        $("#btnDuplicate").trigger("click");
    });
   
    // #endregion ===================================== Cancel Add New =====================================
});// End Document Ready