﻿using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class TransactionDetail
    {
        #region ===================================== GETTERS & SETTERS =====================================

        public int tranId { get; set; }
        public string blockHash { get; set; }
        public string blockNumber { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public int status { get; set; }
        public int uid { get; set; }
        public int itemid { get; set; }

        #endregion ===================================== GETTERS & SETTERS =====================================

        #region ===================================== LẤY TẤT CẢ TransactionDetail =====================================

        /// <summary>  Trả về một danh sách chứa tất cả TransactionDetail</summary>
        /// <returns>Trả về List TransactionDetail</returns>
        public List<TransactionDetail> GetAllTransactionDetail()
        {
            List<TransactionDetail> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<TransactionDetail>();
                query.OrderByDescending(x => x.tranId);
                rows = db.Select(query);
            }
            return rows;
        }

        public List<TransactionDetail> List(int _limit, int _offset, string _search)
        {
            List<TransactionDetail> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<TransactionDetail>();
                int offset = _offset > 0 ? _offset : 0;
                int limit = _limit > 0 ? _limit : 5;
                string search = !string.IsNullOrEmpty(_search) ? _search : "";
                query = query.Where(e => e.blockHash == search || e.blockHash.Contains(search) || e.blockNumber == search || e.blockNumber.Contains(search));
                query.Skip(offset).Take(limit);
                query.OrderByDescending(x => x.tranId);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ===================================== LẤY TẤT CẢ TransactionDetail =====================================

        #region ===================================== XÓA THÔNG QUA tranId=====================================

        /// <summary>Xóa 1 TransactionDetail qua tranId</summary>
        /// <param name="tranId">  Mã tranId của TransactionDetail muốn xóa</param>
        /// <returns>Trả về giá trị đúng hoặc sai</returns>
        public bool Delete(int tranId)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<TransactionDetail>().Where(x => x.tranId == tranId);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }

        #endregion ===================================== XÓA THÔNG QUA tranId=====================================

        #region ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        /// <summary>Cập nhật hoặc thêm mới một đối tượng TransactionDetail</summary>
        /// <param name="TransactionDetail">  Truyền vào là một đối tượng</param>
        /// <returns>Giá trị trả về là 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(TransactionDetail TransactionDetail)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // CODE UPDATE
                if (TransactionDetail.tranId > 0)
                {
                    var queryUpdate = db.From<TransactionDetail>().Where(e => e.tranId == TransactionDetail.tranId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.tranId = TransactionDetail.tranId;
                        objUpdate.blockHash = TransactionDetail.blockHash;
                        objUpdate.blockNumber = TransactionDetail.blockNumber;
                        objUpdate.updated_at = DateTime.Now;
                        objUpdate.uid = TransactionDetail.uid;
                        objUpdate.status = TransactionDetail.status;
                        objUpdate.itemid = TransactionDetail.itemid;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var objInsert = new TransactionDetail
                    {
                        blockHash = TransactionDetail.blockHash,
                        blockNumber = TransactionDetail.blockNumber,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        status = TransactionDetail.status,
                        uid = TransactionDetail.uid,
                        itemid = TransactionDetail.itemid,
                    };
                    result = (int)db.Insert(objInsert, selectIdentity: true);
                }
            }
            return result;
        }

        #endregion ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================
        public List<TransactionDetail> Get(int tranId, string blockHash, string blockNumber)
        {
            List<TransactionDetail> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<TransactionDetail>();
                if (tranId > 0) { query = query.Where(a => a.tranId == tranId); }
                if (!string.IsNullOrEmpty(blockHash)) { query = query.Where(a => a.blockHash == blockHash); }
                if (!string.IsNullOrEmpty(blockNumber)) { query = query.Where(a => a.blockNumber == blockNumber); }
                query.OrderByDescending(x => x.tranId);
                rows = db.Select(query);
            }
            return rows;
        }
    }
}