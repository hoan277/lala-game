﻿using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class SpaceShip
    {
        #region ===================================== GETTERS & SETTERS =====================================

        public int ssId { get; set; }
        public string ssName { get; set; }
        public string ssContent { get; set; }

        #endregion ===================================== GETTERS & SETTERS =====================================

        #region ===================================== LẤY TẤT CẢ  =====================================

        /// <summary>  Trả về một danh sách chứa tất cả Danh mục</summary>
        /// <returns>Trả về List SpaceShip</returns>
        public List<SpaceShip> GetAllSpaceShip()
        {
            List<SpaceShip> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<SpaceShip>();
                query.OrderByDescending(x => x.ssName);
                rows = db.Select(query);
            }
            return rows;
        }

        public List<SpaceShip> List(int _limit, int _offset, string _search)
        {
            List<SpaceShip> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<SpaceShip>();
                int offset = _offset > 0 ? _offset : 0;
                int limit = _limit > 0 ? _limit : 5;
                string search = !string.IsNullOrEmpty(_search) ? _search : "";
                query = query.Where(e => e.ssName == search || e.ssName.Contains(search));
                query.Skip(offset).Take(limit);
                query.OrderByDescending(x => x.ssId);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ===================================== LẤY TẤT CẢ  =====================================

        #region ===================================== XÓA THÔNG QUA ssId=====================================

        /// <summary>Xóa 1 danh mục qua ssId</summary>
        /// <param name="ssId">  Mã ssId của danh mục muốn xóa</param>
        /// <returns>Trả về giá trị đúng hoặc sai</returns>
        public bool Delete(int ssId)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<SpaceShip>().Where(x => x.ssId == ssId);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }

        #endregion ===================================== XÓA THÔNG QUA ssId=====================================

        #region ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        /// <summary>Cập nhật hoặc thêm mới một đối tượng SpaceShip</summary>
        /// <param name="ss">  Truyền vào là một đối tượng</param>
        /// <returns>Giá trị trả về là 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(SpaceShip ss)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // CODE UPDATE
                if (ss.ssId > 0)
                {
                    var queryUpdate = db.From<SpaceShip>().Where(e => e.ssId == ss.ssId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.ssId = ss.ssId;
                        objUpdate.ssName = ss.ssName;
                        objUpdate.ssContent = ss.ssContent;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var objInsert = new SpaceShip
                    {
                        ssName = ss.ssName,
                        ssContent = ss.ssContent,
                    };
                    result = (int)db.Insert(objInsert, selectIdentity: true);
                }
            }
            return result;
        }

        #endregion ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================
        public SpaceShip Get(int ssId, string ssName)
        {
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query= db.From<SpaceShip>();
                if (ssId > 0) { query.Where(x => x.ssId == ssId);}
                if (!string.IsNullOrEmpty(ssName)) { query.Where(x => x.ssName == ssName);}
                return db.Select(query).LastOrDefault();
            }
        }
    }
}