﻿using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class Item
    {
        #region ===================================== GETTERS & SETTERS =====================================

        public int itemId { get; set; }
        public string itemName { get; set; }
        public string itemDesc { get; set; }
        public string itemImage { get; set; }
        public float itemPrice { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public int gameid { get; set; }
        public int status { get; set; }
        public int uid { get; set; }

        #endregion ===================================== GETTERS & SETTERS =====================================

        #region ===================================== LẤY TẤT CẢ Item =====================================

        /// <summary>  Trả về một danh sách chứa tất cả Item</summary>
        /// <returns>Trả về List Item</returns>
        public List<Item> GetAllItem()
        {
            List<Item> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Item>();
                query.OrderByDescending(x => x.itemName);
                rows = db.Select(query);
            }
            return rows;
        }

        public List<Item> List(int _limit, int _offset, string _search)
        {
            List<Item> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Item>();
                int offset = _offset > 0 ? _offset : 0;
                int limit = _limit > 0 ? _limit : 5;
                string search = !string.IsNullOrEmpty(_search) ? _search : "";
                query = query.Where(e => e.itemName == search || e.itemName.Contains(search) || e.itemDesc == search || e.itemDesc.Contains(search));
                query.Skip(offset).Take(limit);
                query.OrderByDescending(x => x.itemId);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ===================================== LẤY TẤT CẢ Item =====================================

        #region ===================================== XÓA THÔNG QUA itemId=====================================

        /// <summary>Xóa 1 Item qua itemId</summary>
        /// <param name="itemId">  Mã itemId của Item muốn xóa</param>
        /// <returns>Trả về giá trị đúng hoặc sai</returns>
        public bool Delete(int itemId)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<Item>().Where(x => x.itemId == itemId);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }

        #endregion ===================================== XÓA THÔNG QUA itemId=====================================

        #region ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        /// <summary>Cập nhật hoặc thêm mới một đối tượng Item</summary>
        /// <param name="Item">  Truyền vào là một đối tượng</param>
        /// <returns>Giá trị trả về là 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(Item Item)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // CODE UPDATE
                if (Item.itemId > 0)
                {
                    var queryUpdate = db.From<Item>().Where(e => e.itemId == Item.itemId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.itemId = Item.itemId;
                        objUpdate.itemName = Item.itemName;
                        objUpdate.itemDesc = Item.itemDesc;
                        objUpdate.itemImage = Item.itemImage;
                        objUpdate.itemPrice = Item.itemPrice;
                        objUpdate.updated_at = DateTime.Now;
                        objUpdate.gameid = Item.gameid;
                        objUpdate.uid = Item.uid;
                        objUpdate.status = Item.status;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var objInsert = new Item
                    {
                        itemName = Item.itemName,
                        itemDesc = Item.itemDesc,
                        itemImage = Item.itemImage,
                        itemPrice = Item.itemPrice,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        gameid = Item.gameid,
                        status = Item.status,
                        uid = Item.uid,
                    };
                    result = (int)db.Insert(objInsert, selectIdentity: true);
                }
            }
            return result;
        }

        #endregion ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================
        public List<Item> Get(int itemId, string itemName, string itemDesc, int gameid)
        {
            List<Item> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Item>();
                if (itemId > 0) { query = query.Where(a => a.itemId == itemId); }
                if (gameid > 0) { query = query.Where(a => a.gameid == gameid); }
                if (!string.IsNullOrEmpty(itemName)) { query = query.Where(a => a.itemName == itemName); }
                if (!string.IsNullOrEmpty(itemDesc)) { query = query.Where(a => a.itemDesc == itemDesc); }
                query.OrderByDescending(x => x.itemId);
                rows = db.Select(query);
            }
            return rows;
        }
        public Item GetDetail(int itemId)
        {
            Item rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Item>();
                if (itemId > 0) { query = query.Where(a => a.itemId == itemId); }
                query.OrderByDescending(x => x.itemId);
                rows = db.Select(query).FirstOrDefault();
            }
            return rows;
        }
    }
}