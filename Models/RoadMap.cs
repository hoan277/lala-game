﻿using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class RoadMap
    {
        #region ===================================== GETTERS & SETTERS =====================================

        public int rmId { get; set; }
        public string rmName { get; set; }
        public string rmContent { get; set; }
        public int rmOrder{ get; set; }

        #endregion ===================================== GETTERS & SETTERS =====================================

        #region ===================================== LẤY TẤT CẢ  =====================================

        /// <summary>  Trả về một danh sách chứa tất cả Danh mục</summary>
        /// <returns>Trả về List RoadMap</returns>
        public List<RoadMap> GetAllRoadMap()
        {
            List<RoadMap> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<RoadMap>();
                query.OrderBy(x => x.rmOrder);
                rows = db.Select(query);
            }
            return rows;
        }

        public List<RoadMap> List(int _limit, int _offset, string _search)
        {
            List<RoadMap> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<RoadMap>();
                int offset = _offset > 0 ? _offset : 0;
                int limit = _limit > 0 ? _limit : 5;
                string search = !string.IsNullOrEmpty(_search) ? _search : "";
                query = query.Where(e => e.rmName == search || e.rmName.Contains(search));
                query.Skip(offset).Take(limit);
                query.OrderByDescending(x => x.rmId);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ===================================== LẤY TẤT CẢ  =====================================

        #region ===================================== XÓA THÔNG QUA rmId=====================================

        /// <summary>Xóa 1 danh mục qua rmId</summary>
        /// <param name="rmId">  Mã rmId của danh mục muốn xóa</param>
        /// <returns>Trả về giá trị đúng hoặc sai</returns>
        public bool Delete(int rmId)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<RoadMap>().Where(x => x.rmId == rmId);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }

        #endregion ===================================== XÓA THÔNG QUA rmId=====================================

        #region ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        /// <summary>Cập nhật hoặc thêm mới một đối tượng RoadMap</summary>
        /// <param name="ss">  Truyền vào là một đối tượng</param>
        /// <returns>Giá trị trả về là 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(RoadMap rm)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // CODE UPDATE
                if (rm.rmId > 0)
                {
                    var queryUpdate = db.From<RoadMap>().Where(e => e.rmId == rm.rmId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.rmId = rm.rmId;
                        objUpdate.rmName = rm.rmName;
                        objUpdate.rmContent = rm.rmContent;
                        objUpdate.rmOrder = rm.rmOrder;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var objInsert = new RoadMap
                    {
                        rmName = rm.rmName,
                        rmContent = rm.rmContent,
                        rmOrder = rm.rmOrder
                };
                    result = (int)db.Insert(objInsert, selectIdentity: true);
                }
            }
            return result;
        }

        #endregion ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================
        public RoadMap Get(int rmId, string rmName)
        {
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query= db.From<RoadMap>();
                if (rmId > 0) { query.Where(x => x.rmId == rmId);}
                if (!string.IsNullOrEmpty(rmName)) { query.Where(x => x.rmName == rmName);}
                return db.Select(query).LastOrDefault();
            }
        }
    }
}