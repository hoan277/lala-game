﻿using Microsoft.Extensions.Configuration;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Reflection;

namespace SEO_System.Models
{
    public class DatabaseUtils
    {
        public static OrmLiteConnectionFactory cnn;
        public static IConfigurationRoot Configuration;
        public static string cnnStr;
        public static bool isDev;
        public DatabaseUtils()
        {
        }
        public static string GetConnectionString(string server)
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            return Configuration[server];
        }

        /// <summary>
        ///   <para>Mở kết nối đến cơ sở dữ liệu</para>
        /// </summary>
        /// <returns>Trả về một IDbConenction</returns>
        public static IDbConnection OpenConnection()
        {
            OrmLiteConfig.DialectProvider = MySqlDialect.Provider;
            OrmLiteConfig.DialectProvider.GetStringConverter().UseUnicode = true;
            if (string.IsNullOrEmpty(cnnStr))
            {
                isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
                cnnStr = GetConnectionString(isDev ? "server" : "local");
            }
            cnn = new OrmLiteConnectionFactory(cnnStr, OrmLiteConfig.DialectProvider);
            IDbConnection dbConnection = null;
            try
            {
                dbConnection = cnn.OpenDbConnection();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Lỗi kết nối đến cơ sở dữ liệu: " + ex.Message);
            }
            return dbConnection;
        }
        public static void SetLicense()
        {
            var licenseUtils = typeof(LicenseUtils);
            var members = licenseUtils.FindMembers(MemberTypes.All, BindingFlags.NonPublic | BindingFlags.Static, null, null);
            Type activatedLicenseType = null;
            foreach (var memberInfo in members)
            {
                if (memberInfo.Name.Equals("__activatedLicense", StringComparison.OrdinalIgnoreCase) && memberInfo is FieldInfo fieldInfo)
                    activatedLicenseType = fieldInfo.FieldType;
            }

            if (activatedLicenseType != null)
            {
                var licenseKey = new LicenseKey
                {
                    Expiry = DateTime.Now.AddYears(100),
                    Ref = "ServiceStack",
                    Name = "Enterprise",
                    Type = LicenseType.Enterprise
                };
                var constructor = activatedLicenseType.GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, new[] { typeof(LicenseKey) }, null);
                if (constructor != null)
                {
                    var activatedLicense = constructor.Invoke(new object[] { licenseKey });
                    var activatedLicenseField = licenseUtils.GetField("__activatedLicense", BindingFlags.NonPublic | BindingFlags.Static);
                    if (activatedLicenseField != null)
                        activatedLicenseField.SetValue(null, activatedLicense);
                }
            }
        }
        public static bool SendEmail(string email, string title, string body, string fileName)
        {
            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress("internationalbusiness2710@gmail.com");
                mail.To.Add(email);
                mail.Subject = title;
                mail.Body = body;
                mail.IsBodyHtml = true;
                if (fileName != null)
                {
                    mail.Attachments.Add(new Attachment(fileName));
                }
                using (SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587))
                {
                    //smtp.Credentials = new NetworkCredential("admin@lalatv.com.vn", "Asd123!@#");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = new NetworkCredential("internationalbusiness2710@gmail.com", "Intlamlt123!");
                    smtp.EnableSsl = true;

                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.Send(mail);
                    return true;
                }
            }
        }
    }
}