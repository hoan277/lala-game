﻿using System.Collections.Generic;

namespace SEO_System.Models
{
    public class Home : DatabaseUtils
    {
        public Dictionary<object, object> CountAll()
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string message = "Thất bại", code = "error";
            try
            {
                int category = new Category().GetAllCategory().Count;
                int user = new User().GetAllUser().Count;
                int role = new Role().GetAllRole().Count;
                int score = new Score().GetAllScore().Count;
                int game = new Game().GetAllGame().Count;
                code = "success";
                message = "Thành công";
                dict.Add("category", category);
                dict.Add("role", role);
                dict.Add("game", game);
                dict.Add("user", user);
                dict.Add("score", score);
            }
            catch (System.Exception ex)
            {
                message = ex.Message;
            }
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }
    }
}