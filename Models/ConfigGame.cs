﻿using Microsoft.AspNetCore.Http;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SEO_System.Models
{
    public class ConfigGame
    {
        #region ===================================== GETTERS & SETTERS =====================================

        public string cgKey { get; set; }
        public string cgValue { get; set; }
        public int cgActive { get; set; }

        #endregion ===================================== GETTERS & SETTERS =====================================

        #region ===================================== LẤY TẤT CẢ DANH MỤC =====================================

        /// <summary>  Trả về một danh sách chứa tất cả Danh mục</summary>
        /// <returns>Trả về List ConfigGame</returns>
        public List<ConfigGame> GetAllConfigGame()
        {
            List<ConfigGame> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<ConfigGame>();
                query.OrderByDescending(x => x.cgKey);
                rows = db.Select(query);
            }
            return rows;
        }

        public List<ConfigGame> List(int _limit, int _offset, string _search)
        {
            List<ConfigGame> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<ConfigGame>();
                int offset = _offset > 0 ? _offset : 0;
                int limit = _limit > 0 ? _limit : 5;
                string search = !string.IsNullOrEmpty(_search) ? _search : "";
                query = query.Where(e => e.cgKey == search || e.cgKey.Contains(search) || e.cgValue == search || e.cgValue.Contains(search));
                query.Skip(offset).Take(limit);
                query.OrderByDescending(x => x.cgActive);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ===================================== LẤY TẤT CẢ DANH MỤC =====================================

        #region ===================================== XÓA THÔNG QUA cgActive=====================================

        /// <summary>Xóa 1 danh mục qua cgActive</summary>
        /// <param name="cgKey">  Mã cgActive của danh mục muốn xóa</param>
        /// <returns>Trả về giá trị đúng hoặc sai</returns>
        public bool Delete(string cgKey)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<ConfigGame>().Where(x => x.cgKey == cgKey);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }

        #endregion ===================================== XÓA THÔNG QUA cgActive=====================================

        #region ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        /// <summary>Cập nhật hoặc thêm mới một đối tượng ConfigGame</summary>
        /// <param name="cg">  Truyền vào là một đối tượng</param>
        /// <returns>Giá trị trả về là 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(ConfigGame cg)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // CODE UPDATE
                if (cg.cgActive > 0)
                {
                    var queryUpdate = db.From<ConfigGame>().Where(e => e.cgKey == cg.cgKey);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.cgActive = cg.cgActive;
                        objUpdate.cgKey = cg.cgKey;
                        objUpdate.cgValue = cg.cgValue;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var objInsert = new ConfigGame
                    {
                        cgKey = cg.cgKey,
                        cgValue = cg.cgValue,
                        cgActive = 1
                    };
                    result = (int)db.Insert(objInsert, selectIdentity: true);
                }
            }
            return result;
        }

        #endregion ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================
        public Dictionary<object, object> upload_img(IFormFile file, string data_model)
        {
            //IFormFile file = Request.Form.Files[0];
            Dictionary<object, object> dict = new();
            string code = "error", message = "Thất bại";

            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            string sFileWillSave = file.FileName;
            string fileDirect = @"/home/amnhacsaigon/dotnet/hag/wwwroot/images/";
            if (isDev)
            {
                fileDirect = @"C://upload";
            }
            if (!Directory.Exists(fileDirect))
            {
                Directory.CreateDirectory(fileDirect);
            }
            var path = Path.Combine(Directory.GetCurrentDirectory(), fileDirect, sFileWillSave);
            if (file.Length > 0)
            {
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyTo(stream);
                    // Update vao db
                    UpdateOrInsert(new ConfigGame()
                    {
                        cgKey = data_model,
                        cgValue = sFileWillSave,
                        cgActive = 1
                    });
                    code = "success";
                    message = "Thành công";

                    dict.Add("result", sFileWillSave);
                }
            }
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }

    }
}