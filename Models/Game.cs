﻿using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class Game
    {
        #region ===================================== GETTERS & SETTERS =====================================

        public int gameId { get; set; }
        public string gameName { get; set; }
        public string gameDesc { get; set; }
        public string gameSlug { get; set; }
        public string gameStatus { get; set; }
        public int cateId { get; set; }

        #endregion ===================================== GETTERS & SETTERS =====================================

        #region ===================================== LẤY TẤT CẢ Game =====================================

        /// <summary>  Trả về một danh sách chứa tất cả Game</summary>
        /// <returns>Trả về List Game</returns>
        public List<Game> GetAllGame()
        {
            List<Game> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Game>();
                query.OrderByDescending(x => x.gameName);
                rows = db.Select(query);
            }
            return rows;
        }

        public List<Game> List(int _limit, int _offset, string _search)
        {
            List<Game> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Game>();
                int offset = _offset > 0 ? _offset : 0;
                int limit = _limit > 0 ? _limit : 5;
                string search = !string.IsNullOrEmpty(_search) ? _search : "";
                query = query.Where(e => e.gameName == search || e.gameName.Contains(search) || e.gameDesc == search || e.gameDesc.Contains(search));
                query.Skip(offset).Take(limit);
                query.OrderByDescending(x => x.gameId);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ===================================== LẤY TẤT CẢ Game =====================================

        #region ===================================== XÓA THÔNG QUA gameId=====================================

        /// <summary>Xóa 1 Game qua gameId</summary>
        /// <param name="gameId">  Mã gameId của Game muốn xóa</param>
        /// <returns>Trả về giá trị đúng hoặc sai</returns>
        public bool Delete(int gameId)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<Game>().Where(x => x.gameId == gameId);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }

        #endregion ===================================== XÓA THÔNG QUA gameId=====================================

        #region ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        /// <summary>Cập nhật hoặc thêm mới một đối tượng Game</summary>
        /// <param name="game">  Truyền vào là một đối tượng</param>
        /// <returns>Giá trị trả về là 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(Game game)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // CODE UPDATE
                if (game.gameId > 0)
                {
                    var queryUpdate = db.From<Game>().Where(e => e.gameId == game.gameId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.gameId = game.gameId;
                        objUpdate.gameName = game.gameName;
                        objUpdate.gameDesc = game.gameDesc;
                        objUpdate.gameSlug = game.gameSlug;
                        objUpdate.gameStatus = game.gameStatus;
                        objUpdate.cateId = game.cateId;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var objInsert = new Game
                    {
                        gameName = game.gameName,
                        gameDesc = game.gameDesc,
                        gameSlug = game.gameSlug,
                        gameStatus = game.gameStatus,
                        cateId = game.cateId,
                    };
                    result = (int)db.Insert(objInsert, selectIdentity: true);
                }
            }
            return result;
        }

        #endregion ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================
        public List<Game> Get(int gameId, string gameName, string gameDesc, string gameSlug, string gameStatus, int cateId)
        {
            List<Game> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Game>();
                if (gameId > 0) { query = query.Where(a => a.gameId == gameId); }
                if (cateId > 0) { query = query.Where(a => a.cateId == cateId); }
                if (!string.IsNullOrEmpty(gameName)) { query = query.Where(a => a.gameName == gameName); }
                if (!string.IsNullOrEmpty(gameDesc)) { query = query.Where(a => a.gameDesc == gameDesc); }
                if (!string.IsNullOrEmpty(gameSlug)) { query = query.Where(a => a.gameSlug == gameSlug); }
                if (!string.IsNullOrEmpty(gameStatus)) { query = query.Where(a => a.gameStatus == gameStatus); }
                query.OrderByDescending(x => x.gameId);
                rows = db.Select(query);
            }
            return rows;
        }
    }
}