﻿using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class Category
    {
        #region ===================================== GETTERS & SETTERS =====================================

        public int cateId { get; set; }
        public string cateName { get; set; }
        public string cateDesc { get; set; }

        #endregion ===================================== GETTERS & SETTERS =====================================

        #region ===================================== LẤY TẤT CẢ DANH MỤC =====================================

        /// <summary>  Trả về một danh sách chứa tất cả Danh mục</summary>
        /// <returns>Trả về List Category</returns>
        public List<Category> GetAllCategory()
        {
            List<Category> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Category>();
                query.OrderByDescending(x => x.cateName);
                rows = db.Select(query);
            }
            return rows;
        }

        public List<Category> List(int _limit, int _offset, string _search)
        {
            List<Category> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Category>();
                int offset = _offset > 0 ? _offset : 0;
                int limit = _limit > 0 ? _limit : 5;
                string search = !string.IsNullOrEmpty(_search) ? _search : "";
                query = query.Where(e => e.cateName == search || e.cateName.Contains(search) || e.cateDesc == search || e.cateDesc.Contains(search));
                query.Skip(offset).Take(limit);
                query.OrderByDescending(x => x.cateId);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ===================================== LẤY TẤT CẢ DANH MỤC =====================================

        #region ===================================== XÓA THÔNG QUA cateId=====================================

        /// <summary>Xóa 1 danh mục qua cateId</summary>
        /// <param name="cateId">  Mã cateId của danh mục muốn xóa</param>
        /// <returns>Trả về giá trị đúng hoặc sai</returns>
        public bool Delete(int cateId)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<Category>().Where(x => x.cateId == cateId);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }

        #endregion ===================================== XÓA THÔNG QUA cateId=====================================

        #region ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        /// <summary>Cập nhật hoặc thêm mới một đối tượng Category</summary>
        /// <param name="cate">  Truyền vào là một đối tượng</param>
        /// <returns>Giá trị trả về là 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(Category cate)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // CODE UPDATE
                if (cate.cateId > 0)
                {
                    var queryUpdate = db.From<Category>().Where(e => e.cateId == cate.cateId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.cateId = cate.cateId;
                        objUpdate.cateName = cate.cateName;
                        objUpdate.cateDesc = cate.cateDesc;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var objInsert = new Category
                    {
                        cateName = cate.cateName,
                        cateDesc = cate.cateDesc
                    };
                    result = (int)db.Insert(objInsert, selectIdentity: true);
                }
            }
            return result;
        }

        #endregion ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================
    }
}