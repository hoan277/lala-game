﻿using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class Score
    {
        #region ===================================== GETTERS & SETTERS =====================================

        public int scoreId { get; set; }
        public int userId { get; set; }
        public string gameSlug { get; set; }
        public int gameLevel { get; set; }
        public int gameScore { get; set; }
        public DateTime? scoreCreated { get; set; }
        public DateTime? scoreUpdated { get; set; }

        #endregion ===================================== GETTERS & SETTERS =====================================

        #region ===================================== LẤY TẤT CẢ Score =====================================

        /// <summary>  Trả về một danh sách chứa tất cả Score</summary>
        /// <returns>Trả về List Score</returns>
        public List<Score> GetAllScore()
        {
            List<Score> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Score>();
                query.OrderByDescending(x => x.scoreId);
                rows = db.Select(query);
            }
            return rows;
        }

        public List<Score> List(int _limit, int _offset, string _search)
        {
            List<Score> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Score>();
                int offset = _offset > 0 ? _offset : 0;
                int limit = _limit > 0 ? _limit : 5;
                if (!string.IsNullOrEmpty(_search))
                {
                    string search = !string.IsNullOrEmpty(_search) ? _search : "";
                    query = query.Where(e => e.scoreId.ToString() == search);
                }
                query.Skip(offset).Take(limit);
                query.OrderByDescending(x => x.scoreId);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ===================================== LẤY TẤT CẢ Score =====================================

        #region ===================================== XÓA THÔNG QUA scoreId=====================================

        /// <summary>Xóa 1 Score qua scoreId</summary>
        /// <param name="scoreId">  Mã scoreId của Score muốn xóa</param>
        /// <returns>Trả về giá trị đúng hoặc sai</returns>
        public bool Delete(int scoreId)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<Score>().Where(x => x.scoreId == scoreId);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }

        #endregion ===================================== XÓA THÔNG QUA scoreId=====================================

        #region ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        /// <summary>Cập nhật hoặc thêm mới một đối tượng Score</summary>
        /// <param name="score">  Truyền vào là một đối tượng</param>
        /// <returns>Giá trị trả về là 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(Score score)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // CODE UPDATE
                if (score.scoreId > 0)
                {
                    var queryUpdate = db.From<Score>().Where(e => e.scoreId == score.scoreId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.scoreId = score.scoreId;
                        objUpdate.userId = score.userId;
                        objUpdate.gameSlug = score.gameSlug;
                        objUpdate.gameLevel = score.gameLevel;
                        objUpdate.gameScore = score.gameScore;
                        objUpdate.scoreCreated = score.scoreCreated ?? DateTime.Now;
                        objUpdate.scoreUpdated = score.scoreUpdated ?? DateTime.Now;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var objInsert = new Score
                    {
                        userId = score.userId,
                        gameSlug = score.gameSlug,
                        gameLevel = score.gameLevel,
                        gameScore = score.gameScore,
                        scoreCreated = score.scoreCreated ?? DateTime.Now,
                        scoreUpdated = DateTime.Now,
                    };
                    result = (int)db.Insert(objInsert, selectIdentity: true);
                }
            }
            return result;
        }

        #endregion ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================
        public List<Score> Get(int scoreId, string gameSlug, int gameLevel, int gameScore, DateTime scoreCreated, DateTime scoreUpdated)
        {
            List<Score> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Score>();
                if (scoreId > 0) { query = query.Where(a => a.scoreId == scoreId); }
                if (gameLevel > 0) { query = query.Where(a => a.gameLevel == gameLevel); }
                if (gameScore > 0) { query = query.Where(a => a.gameScore == gameScore); }
                if (!string.IsNullOrEmpty(gameSlug)) { query = query.Where(a => a.gameSlug == gameSlug); }
                if (scoreCreated.Year > 1999) { query = query.Where(a => a.scoreCreated == scoreCreated); }
                if (scoreUpdated.Year > 1999) { query = query.Where(a => a.scoreUpdated == scoreUpdated); }
                query.OrderByDescending(x => x.scoreId);
                rows = db.Select(query);
            }
            return rows;
        }
    }
}