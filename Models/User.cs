﻿using PasswordHash;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class User
    {
        #region ============================================ GETTER & SETTER =====================================

        public int userId { get; set; }
        public string userName { get; set; }
        public string userPass { get; set; }
        public string userFullName { get; set; }
        public string userDesc { get; set; }
        public string userStatus { get; set; }
        public string userEmail { get; set; }
        public DateTime userBirthday { get; set; }
        public string userGender { get; set; }
        public int roleId { get; set; }
        public int userPoint { get; set; }
        public string userLog { get; set; }

        public List<User> List(int _limit, int _offset, string _search)
        {
            List<User> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<User>();
                int offset = _offset > 0 ? _offset : 0;
                int limit = _limit > 0 ? _limit : 5;
                string search = !string.IsNullOrEmpty(_search) ? _search : "";
                query = query.Where(e => e.userName == search || e.userName.Contains(search) || e.userId.ToString() == search);
                query.Skip(offset).Take(limit);
                query.OrderByDescending(x => x.userId);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ============================================ GETTER & SETTER =====================================

        #region ============================================ GET ALL USER ========================================

        /// <summary>
        /// Lấy tất cả danh sách người dùng
        /// </summary>
        /// <returns>Trả về List<User></returns>
        public List<User> GetAllUser()
        {
            List<User> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<User>();
                query.OrderByDescending(x => x.userName);
                rows = db.Select(query);
            }
            return rows;
        }

        /// <summary>
        /// Lấy tất cả danh sách người dùng theo api
        /// </summary>
        /// <returns>Trả về List<User></returns>
        public List<User> Get(int userId, string userName, string userDesc, string userStatus)
        {
            List<User> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<User>();
                if (userId > 0) { query = query.Where(e => e.userId == userId); }
                if (!string.IsNullOrEmpty(userName)) { query = query.Where(e => e.userName == userName); }
                if (!string.IsNullOrEmpty(userDesc)) { query = query.Where(e => e.userDesc == userDesc); }
                if (!string.IsNullOrEmpty(userStatus)) { query = query.Where(e => e.userStatus == userStatus); }
                query.OrderByDescending(x => x.userName);
                rows = db.Select(query).ToList();
            }
            return rows;
        }

        #endregion ============================================ GET ALL USER ========================================

        #region ===================================== DELETE A USER BY userId=====================================

        /// <summary>
        /// Xóa người dùng qua userId
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>Trả về ĐÚNG hoặc SAI</returns>
        public bool Delete(int userId)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<User>().Where(x => x.userId == userId);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }

        internal User RecoverPass(string userEmail)
        {
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<User>().Where(x => x.userEmail == userEmail);
                User user = db.Select(query).LastOrDefault();
                if (user != null)
                {
                    string newpass = Encrypt.MD5(Guid.NewGuid().ToString().Substring(0, 6));
                    user.userPass = newpass;
                    var body = "Chào <b>" + user.userFullName + "</b>, ";
                    body += "chúng tôi đã xác nhận có một sự thay đổi về mật khẩu của bạn vào lúc <b>" + DateTime.Now.ToString("HH:ss:mm") + " ngày " + DateTime.Now.ToString("dd/mm/yyyy") + "</b><br /><br />"; ;
                    body += "Mật khẩu mới <span style='color:red;font-size:16px;font-weight:600;border:1px solid;padding:10px;'>" + newpass + "</span>";
                    body += "Xin cảm ơn bạn đã sử dụng dịch vụ LALATV Game";
                    DatabaseUtils.SendEmail(userEmail, "Khôi phục mật khẩu Lalatv Game", body, null);
                    UpdateOrInsert(user);
                    return user;
                }
            }
            return null;
        }

        #endregion ===================================== DELETE A USER BY userId=====================================

        #region ===================================== Batch Delete =====================================

        public Dictionary<object, object> BatchDelete(int[] list_selected)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string message = "error", code = "Chưa có item nào được chọn để xóa";
            if (list_selected.Length > 0)
            {
                for (int i = 0; i < list_selected.Length; i++)
                {
                    Delete(list_selected[i]);
                }
                code = "success";
                message = "Xóa thành công";
            }
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }

        #endregion ===================================== Batch Delete =====================================

        #region ===================================== UpdateOrInsert =====================================

        /// <summary>
        /// Cập nhật hoặc thêm mới một đối tượng người dùng
        /// </summary>
        /// <param name="user">Đối tượng User</param>
        /// <returns>Trả về 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(User user)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // CODE UPDATE
                if (user.userId > 0)
                {
                    var queryUpdate = db.From<User>().Where(e => e.userId == user.userId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.userId = user.userId;
                        objUpdate.userName = user.userName;
                        objUpdate.userPass = user.userPass.Length == 32 ? objUpdate.userPass.ToLower() : Encrypt.MD5(user.userPass).ToLower();
                        objUpdate.userFullName = user.userFullName;
                        objUpdate.userStatus = user.userStatus;
                        objUpdate.userBirthday = user.userBirthday;
                        objUpdate.userDesc = user.userDesc;
                        objUpdate.userGender = user.userGender;
                        objUpdate.userEmail = user.userEmail;
                        objUpdate.roleId = user.roleId;
                        objUpdate.userPoint = user.userPoint;
                        objUpdate.userLog = user.userLog;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var query = db.From<User>().Where(e => e.userName == user.userName);
                    var rs = db.Select(query).SingleOrDefault();
                    var objInsert = new User();
                    if (rs != null)
                    {
                        objInsert.userId = rs.userId;
                    }
                    objInsert.userName = user.userName;
                    objInsert.userPass = user.userPass.Length == 32 ? user.userPass.ToLower() : Encrypt.MD5(user.userPass).ToLower();
                    objInsert.userFullName = user.userFullName;
                    objInsert.userStatus = user.userStatus;
                    objInsert.userBirthday = user.userBirthday;
                    objInsert.userDesc = user.userDesc;
                    objInsert.userGender = user.userGender;
                    objInsert.userEmail = user.userEmail;
                    objInsert.roleId = user.roleId;
                    objInsert.userPoint = user.userPoint;
                    objInsert.userLog = user.userLog;
                    if (objInsert.userId > 0)
                    {
                        result = db.Update(objInsert);
                    }
                    else
                    {
                        result = (int)db.Insert(objInsert, selectIdentity: true);
                    }
                }
            }
            return result;
        }

        #endregion ===================================== UpdateOrInsert =====================================

        #region ===================================== Check Login =====================================

        public User CheckLogin(string userName, string userPass)
        {
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<User>().Where(e => (e.userName == userName.ToLower() || e.userEmail == userName.ToLower()) && e.userPass == Encrypt.MD5(userPass));
                var objUser = db.Select(query).SingleOrDefault();
                if (objUser != null)
                {
                    return objUser;
                }
            }
            return null;
        }

        #endregion ===================================== Check Login =====================================

        #region ===================================== UPDATE LOG =====================================

        public string UpdateLog(string userName, string userLog)
        {
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<User>().Where(e => e.userName == userName);
                var objUser = db.Select(query).SingleOrDefault();
                if (objUser != null)
                {
                    var oldLog = objUser.userLog;
                    objUser.userLog = DateTime.Now + " -- " + Environment.MachineName.ToString() + "-- " + userLog + Environment.NewLine + oldLog;
                    try
                    {
                        db.Update(objUser);
                        return "Cập nhật Log thành công";
                    }
                    catch (Exception ex)
                    {
                        return "Lỗi cập nhật Log vào cơ sở dũ liệu: " + ex.Message;
                    }
                }
            }
            return "Cập nhật Log thất bại";
        }

        #endregion ===================================== UPDATE LOG =====================================

        #region ===================================== DELETE LOG =====================================

        public string DeleteLog(int userId)
        {
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<User>().Where(e => e.userId == userId);
                var objUser = db.Select(query).SingleOrDefault();
                if (objUser != null)
                {
                    var oldLog = objUser.userLog;
                    objUser.userLog = "";
                    try
                    {
                        db.Update(objUser);
                        return "Cập nhật Log thành công";
                    }
                    catch (Exception ex)
                    {
                        return "Lỗi cập nhật Log vào cơ sở dũ liệu: " + ex.Message;
                    }
                }
            }
            return "Cập nhật Log thất bại";
        }

        #endregion ===================================== DELETE LOG =====================================

        #region ===================================== IMPORT EXCEL =====================================

        public Dictionary<object, object> ImportExcel()
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string message = "error", code = "Lỗi cập nhật qua Excel";

            code = "success";
            message = "Xóa thành công";
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }

        #endregion ===================================== IMPORT EXCEL =====================================
    }
}