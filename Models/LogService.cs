using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace SEO_System.Models
{
    public class LogService
    {
        public static void WriteTxt(Object obj)
        {
            var method = new StackTrace().GetFrame(1).GetMethod().Name;
            string file = "log_" + DateTime.Now.ToString("yyyyMMdd") + ".log";
            string folder_log = Directory.GetCurrentDirectory() + "/log/";
            if (!Directory.Exists(folder_log))
            {
                Directory.CreateDirectory(folder_log);
            }
            using (StreamWriter w = new StreamWriter(folder_log + file, true, Encoding.UTF8))
            {
                w.WriteLine("============================= " + DateTime.Now + " ============================= function: " + method + " =============================");
                w.WriteLine(nameof(obj) + ":\n" + JsonConvert.SerializeObject(obj, Formatting.Indented));
            }
        }
    }
}