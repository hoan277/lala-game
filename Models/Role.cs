﻿using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class Role
    {
        #region ===================================== GETTERS & SETTERS =====================================

        public int roleId { get; set; }
        public string roleName { get; set; }
        public string roleDesc { get; set; }

        #endregion ===================================== GETTERS & SETTERS =====================================

        #region ===================================== GET ALL Role =====================================

        /// <summary>Lấy tất cả các quyền</summary>
        /// <returns>Trả về một danh sách các quyền</returns>
        public List<Role> GetAllRole()
        {
            List<Role> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Role>();
                query.OrderByDescending(x => x.roleName);
                rows = db.Select(query);
            }
            return rows;
        }

        public List<Role> Get(int roleId, string roleName, string roleDesc)
        {
            List<Role> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Role>();
                if (roleId > 0) { query = query.Where(a => a.roleId == roleId); }
                if (!string.IsNullOrEmpty(roleName)) { query = query.Where(a => a.roleName == roleName); }
                if (!string.IsNullOrEmpty(roleDesc)) { query = query.Where(a => a.roleDesc == roleDesc); }
                query.OrderByDescending(x => x.roleId);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ===================================== GET ALL Role =====================================

        #region ===================================== DELETE A Role BY roleId=====================================

        /// <summary>Xóa một quyền qua roleId</summary>
        /// <param name="roleId">The role identifier.</param>
        /// <returns>Trả về Đúng hoặc Sai</returns>
        public bool Delete(int roleId)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<Role>().Where(x => x.roleId == roleId);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }

        #endregion ===================================== DELETE A Role BY roleId=====================================

        #region ===================================== DELETE A Role BY roleId=====================================

        /// <summary>Cập nhật hoặc thêm mới một quyền</summary>
        /// <param name="role">Đối tượng Role</param>
        /// <returns>Trả về 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(Role role)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // CODE UPDATE
                if (role.roleId > 0)
                {
                    var queryUpdate = db.From<Role>().Where(e => e.roleId == role.roleId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.roleId = role.roleId;
                        objUpdate.roleName = role.roleName;
                        objUpdate.roleDesc = role.roleDesc;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var objInsert = new Role();
                    objInsert.roleName = role.roleName;
                    objInsert.roleDesc = role.roleDesc;
                    result = (int)db.Insert(objInsert, selectIdentity: true);
                }
            }
            return result;
        }

        #endregion ===================================== DELETE A Role BY roleId=====================================
    }
}