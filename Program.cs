using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using SEO_System.Models;

namespace SEO_System
{
    public class Program
    {
        public static void Main(string[] args)
        {
            DatabaseUtils.SetLicense();
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseUrls("http://0.0.0.0:7001");
                })
            ;
    }
}